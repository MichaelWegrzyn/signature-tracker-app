<?php
	include("../includes/db.php");
	$mysqli = new mysqli('localhost', $username, $password, $database);
	$text = $mysqli->real_escape_string($_GET['term']);

	$query = "SELECT * FROM cardsignatures WHERE lname LIKE '%$text%' OR fname LIKE '%$text%' AND received = 0 ORDER BY lname ASC";
	$result = $mysqli->query($query);
	if($result){
		$json = '[';
		$first = true;
		while($row = $result->fetch_assoc())
		{
			if (!$first) { $json .=  ','; } else { $first = false; }
			$json .= '{"value":"'.$row['fullname'].'", "id":"'.$row['id'].'"}';
		}
		$json .= ']';
		echo $json;
	}

	$mysqli->close();
 ?>
