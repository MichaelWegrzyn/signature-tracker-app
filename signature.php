<?php include("includes/db.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Gift Cards Recieved Signatures</title>

	<link href="http://michael-wegrzyn.com/gift-card-tracker/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
	<link href="http://michael-wegrzyn.com/gift-card-tracker/css/styles.css" rel="stylesheet">

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class="wrapper">
		<div class="container-fluid">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#siteNav">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="http://michael-wegrzyn.com/gift-card-tracker/">Gift Card Tracker</a>
					</div>

					<div class="collapse navbar-collapse" id="siteNav">
						<ul class="nav navbar-nav">
							<li class=""><a href="http://michael-wegrzyn.com/gift-card-tracker/">Home</a></li>
							<li class="active"><a href="http://michael-wegrzyn.com/gift-card-tracker/signature.php">Received Gift Cards</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>

			<?php
				$connectDB = mysql_connect('localhost', $username, $password);
				$mysqli = new mysqli('localhost', $username, $password, $database);

				$query = "SELECT * FROM cardsignatures WHERE received = 1 ORDER BY lname";
				$result = $mysqli->query($query);

				// if($result === FALSE) {
				// 	die(mysql_error()); //TODO: better error handling
				// }

				while($row= mysqli_fetch_array($result)){
					echo "<p><a href='#' id='" .$row['id']. "' class='showSig'>" .$row['fullname']. "</a></p>";
				}

				$mysqli->close();
			 ?>

			<div class="modal fade" id="signatureModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel"><span id="signatureName"></span> Signature</h4>
						</div>
						<div class="modal-body">
							<img src="" alt="signature" id="signatureInsert" style="max-width:100%;" />
						</div>
					</div>
				</div>
			</div>

		</div><!--- END CONTAINER FLUID -->

	</div><!--- END WRAPPER -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script src="http://michael-wegrzyn.com/gift-card-tracker/js/bootstrap.min.js"></script>
	<script src="http://michael-wegrzyn.com/gift-card-tracker/js/signature_pad.min.js"></script>
	<script src="http://michael-wegrzyn.com/gift-card-tracker/js/site.js"></script>
</body>
</html>
