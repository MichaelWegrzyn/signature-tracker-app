$( document ).ready(function() {

	$('#findUser').val('');

	$('#findUser').autocomplete({
		source: "php/search.php",
		minLength: 3,
		select: function(event, ui) {
			var selectedUser = ui.item;
			$(this).val(selectedUser.value);
			$("#userId").val(selectedUser.id);
			// alert($("#findUser").val());
			// alert($("#userId").val());
		}
	});

	$("#continueUser").click(function() {

		// alert($("#findUser").val());
		// alert($("#userId").val());

		var user = $("#findUser").val();
		var userid = $("#userId").val();

		 if(user && userid){
			// alert(user);
			// alert(userid);
			$.ajax({
				url: 'php/findUser.php',
				type: 'GET',
				dataType: 'json',
				data: {user: user, userId: userid},
				success: function(json){
					$("#searchForUser").hide();
					$("#username").text(json.name);
					$("#updateUserId").val(json.id);
					$("#userSignature").fadeIn();

					var wrapper = document.getElementById("signaturePad")
					var canvas = wrapper.querySelector("canvas")
					var signaturePad;


					$('#clearSignature').click(function() {
						signaturePad.clear();
					});
					$('#saveSignature').click(function(event) {
						if (signaturePad.isEmpty()) {
							alert("Please provide signature first.");
						} else {
							var pngData = signaturePad.toDataURL().split(",")[1];
							//alert(pngData);
							var userId = $("#updateUserId").val();

							$.ajax({
								url: 'php/updateUser.php',
								type: 'POST',
								dataType: 'json',
								data: {userId: userId, pngData: pngData },
								success: function(json){
									if(json.status){
										alert('Users signature has been saved!');
										window.location = '/gift-card-tracker/';

									}else{
										alert('ERROR: Users signature has not been saved, please clear signature field and try again.');
									}

								}
							});
						}
					});

					function resizeCanvas() {
						var ratio = window.devicePixelRatio || 1;
						canvas.width = canvas.offsetWidth * ratio;
						canvas.height = canvas.offsetHeight * ratio;
						canvas.getContext("2d").scale(ratio, ratio);
					}
					window.onresize = resizeCanvas;
					resizeCanvas();
					signaturePad = new SignaturePad(canvas);

				}
			})

		}else{
			alert('Invalid User Selected');
		}
	});

	$('.showSig').click(function() {

		var userId = $(this).attr('id');
		$("#signatureName").text($(this).text());
		$.ajax({
			url: 'php/showSignature.php',
			type: 'GET',
			dataType: 'json',
			data: {userId: userId},
			success: function(json){
				if(json.signature){
					var signatureImage ="data:image/png;base64,"+json.signature;
					//alert(signatureImage);
					$("#signatureInsert").attr("src", signatureImage);
					$("#signatureModal").modal();

				}
			}
		});

	});

});
