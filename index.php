<?php include("includes/db.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Gift Cards Recieved Signatures</title>

	<!-- Bootstrap core CSS -->
	<link href="http://michael-wegrzyn.com/gift-card-tracker/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
	<link href="http://michael-wegrzyn.com/gift-card-tracker/css/styles.css" rel="stylesheet">

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class="wrapper">
		<div class="container-fluid">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#siteNav">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="http://michael-wegrzyn.com/gift-card-tracker/">Gift Card Tracker</a>
					</div>

					<div class="collapse navbar-collapse" id="siteNav">
						<ul class="nav navbar-nav">
							<li class="active"><a href="http://michael-wegrzyn.com/gift-card-tracker/">Home</a></li>
							<li class=""><a href="http://michael-wegrzyn.com/gift-card-tracker/signature.php">Received Gift Cards</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>


			<div class="row" id="searchForUser">
				<div class="col-xs-12">
					<h1>Gift Card Tracker</h1>
				</div>

				<div class="col-xs-9">
					<input type="text"  class="form-control" name="findUser" id="findUser" placeholder="Search by First or Last Name" />
					<input type="hidden" id="userId" name="userId" />
					<p class="helperInfo text-muted small"><em>Search for the user by first or last name.  Once a User has been selected, select "continue", you will be sent to the page for the user selected to sign saying they have received their credit card.</em></p>
				</div>
				<div class="col-xs-3">
					<button type="button" class="btn btn-success form-control" id="continueUser">Continue</button>
				</div>
			</div><!-- end ROW -->

			<div id="userSignature" class="row" style="display:none;">
				<div class="col-xs-12">
					<h1>Congratulations <span id="username"></span>!</h1>
					<p>Please sign below to acknowledge that you have recieved your gift card.</p>

					<div id="signaturePad">
						<canvas></canvas>
					</div>
					<input type="hidden" id="updateUserId" name="updateUserId" value="" />
					<div class="sigBtns">
						<button id="saveSignature" class="btn  btn-success">Save Signature</button>
						<button id="clearSignature" class="btn btn-danger">Clear</button>
					</div>
				</div>
			</div>

		</div><!--- END CONTAINER FLUID -->

	</div><!--- END WRAPPER -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script src="http://michael-wegrzyn.com/gift-card-tracker/js/bootstrap.min.js"></script>
	<script src="http://michael-wegrzyn.com/gift-card-tracker/js/signature_pad.min.js"></script>
	<script src="http://michael-wegrzyn.com/gift-card-tracker/js/site.js"></script>
</body>
</html>
